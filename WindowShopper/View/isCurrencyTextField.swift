//
//  isCurrentTextField.swift
//  WindowShopper
//
//  Created by Douglas Spencer on 7/13/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

@IBDesignable
class isCurrencyTextField: UITextField {
    
    override func draw(_ rect: CGRect) {
        CreateCurrencyLabel()
    }
    
    func CreateCurrencyLabel() {
        let size: CGFloat = 20
    
        let FrameForLabel = CGRect(x: 5, y: (frame.size.height / 2) - size / 2, width: size, height: size)
    
        let lblCurrency: UILabel = UILabel(frame: FrameForLabel)
    
        lblCurrency.backgroundColor = #colorLiteral(red: 0.8173284151, green: 0.8173284151, blue: 0.8173284151, alpha: 0.7981571127)
        lblCurrency.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        lblCurrency.textAlignment = .center
        
        lblCurrency.layer.cornerRadius = 5.0
        lblCurrency.clipsToBounds = true
    
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        lblCurrency.text = formatter.currencySymbol
    
        lblCurrency.clipsToBounds = true
    
        addSubview(lblCurrency)
    }
    
    
    override func prepareForInterfaceBuilder() {
        SetupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func SetupView() {
        self.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.2485621166)
        self.layer.cornerRadius = 5.0
        self.textAlignment = .center
        
        if placeholder == nil {
            placeholder = ""
        } else {
            
        }
        
        let place = NSAttributedString(string: placeholder!, attributes: [.foregroundColor:UIColor.white])
        attributedPlaceholder = place
        textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        clipsToBounds = true
        
    }
    
}
