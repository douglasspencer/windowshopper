//
//  Wage.swift
//  WindowShopper
//
//  Created by Douglas Spencer on 7/13/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import Foundation

class Wage {
    class func getHoursForWage(forWage wage: Double, forPrice price: Double) -> Int {
        return Int(ceil(price/wage))
    }
}
