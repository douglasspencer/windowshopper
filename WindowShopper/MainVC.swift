//
//  ViewController.swift
//  WindowShopper
//
//  Created by Douglas Spencer on 7/12/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var txtHourlyWage: isCurrencyTextField!
    @IBOutlet weak var txtItemPrice: isCurrencyTextField!
    
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblResult: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CreateCalcuateButton()
        SetupUIForInitialUse()
    }

    @objc func Calculate() {
        print("Calculate Me")
        if let givenWageText = txtHourlyWage.text,  let givenPriceText = txtItemPrice.text {
            if let givenWage = Double(givenWageText), let givenPrice = Double(givenPriceText) {
                view.endEditing(true)
                let iResult = Wage.getHoursForWage(forWage: givenWage, forPrice: givenPrice)
                lblResult.text = String(iResult)
                
                lblResult.isHidden = false
                lblHours.isHidden = false
                
                UIView.animate(withDuration: 1.0, animations: {
                    self.lblResult.alpha = 1.0
                    self.lblHours.alpha = 1.0
                })
            }
        }
    }
    
    func CreateCalcuateButton() {
        // Do any additional setup after loading the view, typically from a nib.
        
        let frameForButton: CGRect = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60)
        
        let calcButton = UIButton(frame: frameForButton)
        calcButton.backgroundColor = #colorLiteral(red: 1, green: 0.5415005739, blue: 0.1077083692, alpha: 1)
        calcButton.setTitle("Calculate", for: .normal)
        calcButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        calcButton.addTarget(self, action: #selector(MainVC.Calculate), for: .touchUpInside)
        
        txtHourlyWage.inputAccessoryView = calcButton
        txtItemPrice.inputAccessoryView = calcButton
    }
    
     func SetupUIForClearCalculator() {
        lblResult.text = ""
        lblResult.isHidden = true
        lblHours.isHidden = true
        txtHourlyWage.text = ""
        txtItemPrice.text = ""
    }
    
    func SetupUIForInitialUse() {
        //Set result labels to be invisible, we will animate them in when we calcuate the result
        lblResult.alpha = 0.0
        lblHours.alpha = 0.0
    }
    
    @IBAction func ClearCalculator() {
        SetupUIForClearCalculator()
    }
    
    
    
}

